/**
Refer to header file "function.h" for a complete tutorial on all
classes, member functions, and functions.
*/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <vector>
#include <iomanip>
#include <cmath>
#include "Function.h"
using namespace std;

Function::Function()
{
	expression = "";
}

Function::Function(string equation)
{
	expression = equation;
}

double Function::output(double x)
{
	int len = 0;
	return parse_expression(expression + " ", len, x, 0);
}

string Function::get_function() const
{
	return expression;
}

void Function::change_function(string new_func)
{
    expression = new_func;
}

//To use this function you must create a vector in main() with the x-coordinates used to create the interpolating polynomial
//This vector must be passed as a parameter
double Function::Lagrange(vector <double> nodes, double x)
{	
	vector <double> Ls(nodes.size());
	double lagrange = 0;
	for (int i = 0; i < nodes.size(); i++)//Calculates f(x)L(x)
	{
		int lon = 0;
		double numerator = 1;
		double denominator = 1;
		for (int j = 0; j < nodes.size(); j++)//Calculates L(x)
		{
			if (i != j)
			{	
				int lan = 0;
				numerator =	numerator*(parse_expression("x ", lan, x, 0) - nodes[j]);
				denominator = denominator*(nodes[i] - nodes[j]);
				Ls[i] = numerator/denominator;
			}
		}
		lagrange = lagrange + parse_expression(expression + " ", lon, nodes[i], 0)*Ls[i];
	}
	return lagrange;
}

//Differentiation Member Functions
//These generally approximate more accurate values for a samller sized increment (0.1 or 0.01)
double Function::der_3pt(double increment, double input)//Three-point Midpoint Formula
{
	int len = 0, lan = 0;
	return (1/(2*increment))*(parse_expression(expression + " ", len, input + increment, 0) - parse_expression(expression + " ", lan, input - increment, 0));
}

double Function::der_5pt(double increment, double input)//Five-Point Midpoint Formula
{
	int lan = 0, len = 0, lin = 0, lon = 0;
	return (1/(12*increment))*(parse_expression(expression + " ", lan, input - 2*increment, 0) - 8*parse_expression(expression + " ", len, input - increment, 0) 
		+ 8*parse_expression(expression + " ", lin, input + increment, 0) - parse_expression(expression + " ", lon, input + 2*increment, 0));
}

double Function::second_der(double increment, double input)//2nd Derivative Midpoint Formula 
{
	int lan = 0, len = 0, lin = 0;
	return (1/(increment*increment))*(parse_expression(expression + " ", lan, input  - increment, 0) - 2*parse_expression(expression + " ", len, input, 0)
		+ parse_expression(expression + " ", lin, input + increment, 0));
}

//Integration Member Function
//Approximates integral from 'interval_beg' to 'interval_end'. Generally more accurate for larger 'num_nodes'.
double Function::comp_simpsons(double interval_beg, double interval_end, int num_nodes)
{
	int lan = 0, lun = 0;
	double increment = (interval_end - interval_beg)/num_nodes;
	double integral_approx_0 = parse_expression(expression + " ", lan, interval_beg, 0) + parse_expression(expression + " ", lun, interval_end, 0);
	double integral_approx_1 = 0, integral_approx_2 = 0;

	for (int i = 1; i <= num_nodes - 1; i++)
	{
		int len = 0, lon = 0;
		double node = interval_beg + i*increment;
		if (i%2 == 0)
			integral_approx_2 = integral_approx_2 + parse_expression(expression + " ", len, node, 0);
		else
			integral_approx_1 = integral_approx_1 + parse_expression(expression + " ", lon, node, 0);
	}
	return (increment*(integral_approx_0 + 2*integral_approx_2 + 4*integral_approx_1))/3;
}

IVP::IVP(string equation, double a, double b, double ic) : Function(equation)
{
	interval_beg = a;
	interval_end = b;
	init_cond = ic;
}

/*Approximates solution to an initial value problem using the Runge Kutta Order 4 Method.
The interval of the IVP is divided into (N+1) nodes and a solution is constructed
from an approximation for each node*/
double IVP::Runge_Kutta(int N)
{
	double h = (interval_end - interval_beg)/N;
	double t = interval_beg;
	double w = init_cond;

	double k_1, k_2, k_3, k_4;
	for (int i = 1; i<= N; i++)
	{
		int lan = 0, len = 0, lin = 0, lon = 0;
		k_1 = h*parse_expression(get_function() + " ", lan, t, w);
		k_2 = h*parse_expression(get_function() + " ", len, t + h/2, w + k_1/2);
		k_3 = h*parse_expression(get_function() + " ", lin, t + h/2, w + k_2/2);
		k_4 = h*parse_expression(get_function() + " ", lon, t + h, w + k_3);
		w = w + (k_1 + 2*k_2 + 2*k_3 + k_4)/6;
		t = interval_beg + i*h;
		cout << setw(3) << t << setw(10) << w << endl;
	}
	return w;
}


Matrix::Matrix ()
{
	rows = 0;
	columns = 0;
}

Matrix::Matrix (int n, int m)
{
	rows = n;
	columns = m;
}

void Matrix::fill_matrix()
{
	cout << "Fill your " << rows << "x" << columns << " matrix by row:\n";
	for (int i = 0; i <= rows - 1; i++)
	{
		double number = 0;
		vector <double> a(columns);
		for (int j = 0; j <= columns - 1; j++)
		{
			cin >> number;
			a[j] = number;
		}
		matrix.push_back(a);
	}
}

void Matrix::output_matrix()
{
	for (int i = 0; i <= rows - 1;i++)
	{
		for (int j = 0; j <= columns - 1; j++)
		{
			cout << matrix[i][j] << "   ";
		}
		cout << endl;
	}
}

vector <vector <double> > Matrix::get_matrix()
{
	return matrix;
}

void Matrix::alter_matrix(int i, int j, double value)
{
	matrix[i][j] = value;
}

void Matrix::Gauss_Seidel(int max_iter)
{
	Matrix b(rows, 1);
	Matrix initial_approx(rows, 1);
	b.fill_matrix();
	initial_approx.fill_matrix();//usually the zero vector
	
	int k = 1;
	vector <double> x(rows);
	while (k <= max_iter)
	{
		for (int i = 0; i <= rows - 1; i++)
		{
			if (i == 0)
			{
				double sum1 = 0;
				for (int j = 1; j <= rows - 1; j++)
                    sum1 = sum1 + (matrix[i][j])*(initial_approx.get_matrix()[j][0]);
				x[i] = (1/matrix[i][i])*(-sum1 + b.get_matrix()[i][0]);
			}
			else
			{
				double sum2 = 0, sum3 = 0;
				for (int j = 0; j <= i - 1; j++)
					sum2 = sum2 + matrix[i][j]*x[j];
				for (int j = i + 1; j <= rows - 1; j++)
					sum3 = sum3 + matrix[i][j]*initial_approx.get_matrix()[j][0];
				x[i] = (1/matrix[i][i])*(-sum2 - sum3 + b.get_matrix()[i][0]);
			}
		}
		k = k + 1;
		for (int i = 0; i <= rows - 1; i++)
			initial_approx.alter_matrix(i, 0, x[i]);
	}
	for(int i = 0; i <= rows - 1; i++)
	{
		if (i < rows - 1)
			cout << x[i] << ", ";
		else
			cout << x[i];
	}
}

double Newtons(Function a, double init_approx, double TOL, int max_iter)
{
	int i = 1;
	double approximation = init_approx;
	while (i <= max_iter)
	{
		int lan = 0;
		approximation = init_approx - a.output(init_approx)/a.der_5pt(100, init_approx);
		if(fabs(approximation - init_approx) < TOL)
			return approximation;
		i = i + 1;
		init_approx = approximation;
	}
	return approximation;
}

double fixed_point(Function a, double init_approx, double TOL, int max_iter)
{
	int i = 1;
	double approximation = init_approx;
	while (i <= max_iter)
	{
		approximation = a.output(init_approx);
		if (fabs(approximation - init_approx) < TOL)
			return approximation;
		i = i + 1;
		init_approx = approximation;
	}
	return approximation;
}

double secant_method(Function a, double init_approx0, double init_approx1, double TOL, int max_iter)
{
	int i = 2;
	double q_0 = a.output(init_approx0), q_1 = a.output(init_approx1);
	double approximation = 0;
	while (i <= max_iter)
	{
		approximation = init_approx1 - q_1*(init_approx1 - init_approx0)/(q_1 - q_0);
		if(fabs(approximation - init_approx1) < TOL)
			return approximation;
		i = i + 1;
		init_approx0 = init_approx1;
		q_0 = q_1;
		init_approx1 = approximation;
		q_1 = a.output(approximation);
	}
	return approximation;
}
		
/*This is where the parser begins. It evaluates a math expression recursively by breaking it
down into expressions, terms, and factors.(Refer to mandatory rules at the top of this page.)*/
double parse_factor(string input, int& len, double x, double y)
{
	if(input[len] == '(')
	{	
		len++;
		double h = parse_expression(input, len, x, y);
		return h;
	}
	if(input[len] == '-')
	{
		len++;
		return -(parse_factor(input, len, x, y));
	}
	if (input[len] == 'a')
	{
		len = len + 3;
		double abs = fabs(parse_expression(input, len, x, y));
		if (len != input.size() - 1)
			len++;
		return abs;
	}
	if (input[len] == 'e')
	{
		len = len + 3;
		double exponential = exp(parse_expression(input, len, x, y));
		if (len != input.size() - 1)
			len++;
		return exponential;
	}
	if (input[len] == 'l')
	{
		len++;
		if (input[len] == 'o')
		{
			len = len + 2;
			double log_x = log10(parse_expression(input, len, x, y));
			if (len != input.size() - 1)
				len++;
			return log_x;
		}
		if (input[len] == 'n')
		{
			len = len + 1;
			double ln = log(parse_expression(input, len, x, y));
			if (len != input.size() - 1)
				len++;
			return ln;
		}
	}
	if (input[len] == 's')
	{
		len = len + 3;
		double sin_x = sin(parse_expression(input, len, x, y));
		if (len != input.size() - 1)
			len++;
		return sin_x;
	}
	if (input[len] == 'c')
	{
		len = len + 3;
		double cos_x = cos(parse_expression(input, len, x, y));
		if (len != input.size() - 1)
			len++;
		return cos_x;
	}
	if (input[len] == 't')
	{
		len = len + 3;
		double tan_x = tan(parse_expression(input, len, x, y));
		if (len != input.size() - 1)
			len++;
		return tan_x;
	}
	if (input[len] == 'x')
	{
		if (len != input.size() - 1)
			len++;
		return x;
	}
	if (input[len] == 'y')
	{
		if (len != input.size() - 1)
			len++;
		return y;
	}
	string number = "";
	while (input[len] == '0' || input[len] == '1' || input[len] == '2' || input[len] == '3' || input[len] =='4' || input[len] =='5' || 
		input[len] == '6' || input[len] =='7' || input[len] =='8' || input[len] == '9' || input[len] == '.') 
	{
		number = number + input[len];
		len++;
	}
	if (input[len] == ')')
	{
		return atof(number.c_str());
	}
	return atof(number.c_str());
}

double parse_term(string input, int& len, double x, double y)
{
	double v1 = parse_factor(input, len, x, y);
	if (input[len] == '*')
	{
		len++;
		double v2 = parse_term(input, len, x, y);
		if (len != input.size() - 1 && input[len] == ')')
			len++;
		return v1*v2;
	}
	if (input[len] == '/')
	{
		len++;
		double v2 = parse_term(input, len, x, y);
		if (len != input.size() - 1 && input[len] == ')')
			len++;
		return v1/v2;
	}
	if (input[len] == '^')
	{
		len ++;
		double v2 = parse_term(input, len, x, y);
		if (len != input.size() - 1 && input[len] == ')')
			len++;
		return pow(v1, v2);
	}
	return v1;
}

double parse_expression(string input, int& len, double x, double y)
{
	double v1 = parse_term(input, len, x, y);
	if(input[len] == '+')
	{
		len++;
		double v2 = parse_expression(input, len, x, y);
		if (len != input.size() - 1 && input[len] == ')')
			len++;
		return v1 + v2;
	}
	if (input [len] == '-')
	{
		len++;
		double v2 = parse_expression(input, len, x, y);
		if (len != input.size() - 1 && input[len] == ')')
			len++;
		return v1 - v2;
	}
	return v1;
}
//End of parser
