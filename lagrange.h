#ifndef LAGRANGE_H
#define LAGRANGE_H

#include <QDialog>

namespace Ui {
class LaGrange;
}

class LaGrange : public QDialog
{
    Q_OBJECT
    
public:
    explicit LaGrange(QWidget *parent = 0);
    ~LaGrange();
    void alterlineEdit(QString new_string);

private slots:
    void lagrange_output();

private:
    Ui::LaGrange *ui;
};

#endif // LAGRANGE_H
