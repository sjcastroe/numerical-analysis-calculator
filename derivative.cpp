#include "derivative.h"
#include "ui_derivative.h"
#include "mainwindow.h"

Derivative::Derivative(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Derivative)
{
    ui->setupUi(this);
}

Derivative::~Derivative()
{
    delete ui;
}

void Derivative::alterlineEdit(QString new_string)
{
    ui -> lineEdit -> setText(new_string);
}

void Derivative::func_der()
{
    QString expression = ui->lineEdit->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    Function a(exp_copy);
    double input = ui -> lineEdit_2 ->text().toDouble();
    double answer = a.der_5pt(0.1, input);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_3 -> setText(str_answer);
}
