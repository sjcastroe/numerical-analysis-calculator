#ifndef INTEGRAL_H
#define INTEGRAL_H

#include <QDialog>

namespace Ui {
class Integral;
}

class Integral : public QDialog
{
    Q_OBJECT
    
public:
    explicit Integral(QWidget *parent = 0);
    ~Integral();
    void alterlineEdit(QString new_string);
    
private slots:
    void func_integral();

private:
    Ui::Integral *ui;
};

#endif // INTEGRAL_H
