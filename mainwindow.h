#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include "function.h"
#include "output.h"
#include "lagrange.h"
#include "derivative.h"
#include "second_derivative.h"
#include "integral.h"
#include "zero.h"
#include "rungekutta.h"
#include "about.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void input_output();
    void lagrange();
    void derivative();
    void second_derivative();
    void integral();
    void zero();
    void rungekutta();
    void on_actionAbout_triggered();

private:
    Ui::MainWindow *ui;
    About *info;
};

#endif // MAINWINDOW_H
