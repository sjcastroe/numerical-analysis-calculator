#include "integral.h"
#include "ui_integral.h"
#include "mainwindow.h"

Integral::Integral(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Integral)
{
    ui->setupUi(this);
}

Integral::~Integral()
{
    delete ui;
}

void Integral::alterlineEdit(QString new_string)
{
    ui -> lineEdit_7 -> setText(new_string);
}

void Integral::func_integral()
{
    QString expression = ui->lineEdit_7 ->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    Function a(exp_copy);
    double lower_bound = ui -> lineEdit_8 ->text().toDouble();
    double upper_bound = ui -> lineEdit_9 ->text().toDouble();
    double nodes = ui -> lineEdit_10 ->text().toDouble();
    double answer = a.comp_simpsons(lower_bound, upper_bound, nodes);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_11 -> setText(str_answer);
}
