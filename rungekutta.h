#ifndef RUNGEKUTTA_H
#define RUNGEKUTTA_H

#include <QDialog>

namespace Ui {
class RungeKutta;
}

class RungeKutta : public QDialog
{
    Q_OBJECT
    
public:
    explicit RungeKutta(QWidget *parent = 0);
    ~RungeKutta();
    void alterlineEdit(QString new_string);

private slots:
    void RKutta();

private:
    Ui::RungeKutta *ui;
};

#endif // RUNGEKUTTA_H
