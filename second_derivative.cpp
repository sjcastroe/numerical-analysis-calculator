#include "second_derivative.h"
#include "ui_second_derivative.h"
#include "mainwindow.h"

Second_Derivative::Second_Derivative(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Second_Derivative)
{
    ui->setupUi(this);
}

Second_Derivative::~Second_Derivative()
{
    delete ui;
}

void Second_Derivative::alterlineEdit(QString new_string)
{
    ui -> lineEdit -> setText(new_string);
}

void Second_Derivative::func_2nd_der()
{
    QString expression = ui->lineEdit->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    Function a(exp_copy);
    double input = ui -> lineEdit_2 ->text().toDouble();
    double answer = a.second_der(0.1, input);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_3 -> setText(str_answer);
}
