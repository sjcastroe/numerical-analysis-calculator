#ifndef SECOND_DERIVATIVE_H
#define SECOND_DERIVATIVE_H

#include <QDialog>

namespace Ui {
class Second_Derivative;
}

class Second_Derivative : public QDialog
{
    Q_OBJECT
    
public:
    explicit Second_Derivative(QWidget *parent = 0);
    ~Second_Derivative();
    void alterlineEdit(QString new_string);

public slots:
    void func_2nd_der();
    
private:
    Ui::Second_Derivative *ui;
};

#endif // SECOND_DERIVATIVE_H
