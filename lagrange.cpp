#include "lagrange.h"
#include "ui_lagrange.h"
#include "mainwindow.h"
#include <string>
#include <stdlib.h>
#include <vector>

LaGrange::LaGrange(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LaGrange)
{
    ui->setupUi(this);
}

LaGrange::~LaGrange()
{
    delete ui;
}

void LaGrange::alterlineEdit(QString new_string)
{
    ui -> lineEdit -> setText(new_string);
}

void LaGrange::lagrange_output()
{
    QString expression = ui->lineEdit->text();
    std::string exp_copy = expression.toLocal8Bit().constData();
    Function a(exp_copy);
    QString coordinates = ui -> lineEdit_2 -> text();
    std::string str_coord = coordinates.toLocal8Bit().constData();
    size_t str_coord_size = str_coord.size();
    vector <double> coord(0);
    for (int i = 0; i < str_coord_size;i++)
    {
        string number = "";
        while(str_coord[i] != ',' && i < str_coord_size)
        {
            number = number + string(1, str_coord[i]);
            i++;
        }
        coord.push_back(atof(number.c_str()));
    }
    double input = ui -> lineEdit_3 ->text().toDouble();
    double answer = a.Lagrange(coord, input);
    QString str_answer = QString::number(answer);
    ui -> lineEdit_4 -> setText(str_answer);
}
