#-------------------------------------------------
#
# Project created by QtCreator 2013-06-05T00:12:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qt_Practice1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Parser.cpp \
    output.cpp \
    lagrange.cpp \
    derivative.cpp \
    second_derivative.cpp \
    integral.cpp \
    zero.cpp \
    rungekutta.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    function.h \
    output.h \
    lagrange.h \
    derivative.h \
    second_derivative.h \
    integral.h \
    zero.h \
    rungekutta.h \
    about.h

FORMS    += mainwindow.ui \
    output.ui \
    lagrange.ui \
    derivative.ui \
    second_derivative.ui \
    integral.ui \
    zero.ui \
    rungekutta.ui \
    about.ui
