#ifndef DERIVATIVE_H
#define DERIVATIVE_H

#include <QDialog>

namespace Ui {
class Derivative;
}

class Derivative : public QDialog
{
    Q_OBJECT
    
public:
    explicit Derivative(QWidget *parent = 0);
    ~Derivative();
    void alterlineEdit(QString new_string);
    
private slots:
    void func_der();

private:
    Ui::Derivative *ui;
};

#endif // DERIVATIVE_H
